# 03.03.Visualizacion.de.la.informacion

Días:
* Miércoles de 18 a 20 hs. 	
	* Dos bloques de 1 hora
	* Segunda parte gráficos y codeo
* Sábados de 10 a 12 hs. / 13 a 15 hs. (Cada 15 días)

**Zoom**

	https://exactas-uba.zoom.us/j/83912275946
	ID de reunión: 839 1227 5946
	Código de acceso: vi22-lgnf


## Profesores

* Claudio Delrieux – cad@uns.edu.ar 
	* Laboratorio de Ciencias de las Imágenes – www.imaglabs.org
	* Universidad Nacional del Sur – www.uns.edu.ar
* Germán Rosati - german.rosati@gmail.com
	* EIDAES-UNSAM / CONICET
* Paula Feldman - paulaadifeldman1@gmail.com
	* CONICET


## Herramientas y comunicacion

* Observable -> Simil "Jupyter notebook"
	* No python ni R. Un lenguaje bajado en JS.
	* Refs:
		* https://observablehq.com/
		* https://observablehq.com/@observablehq/introduction-to-code
* Classroom: http://157.92.26.246/campus/course/view.php?id=9
* Slack?
* Facebook: https://www.facebook.com/groups/vi2022
* Libros
	* Tufte
	* https://magrawala.github.io/cs448b-wi20/ (Usado como referencia)

## Reglamentaciones

* 80% presenciales en teoricos y prácticos
	* ¿?
	* 8 sábados - 2 * 8 míercoles imagino => 24 clases (20 clases asistencia - Duro)
	* Se adaptan a nosotros a priori. La última hacerla presencial.
* 4 entregas de prácticas
* Trabajo final - VAST Challenge
	* http://www.vacommunity.org/About+the+VAST+Challenge
	* Grupos de 5 personas. Tomar uno de los 3 desafíos y resolver.

## Presencialidad


- La clase próxima (30/03) será en formato VIRTUAL
- La clase del 06/04 será en formato VIRTUAL
- La clase del 13/04 (en la que comenzaremos con Observable y D3) será en formato PRESENCIAL

En función de cómo avancemos con la parte práctica (de Observable y D3), veremos cómo podemos distribuir el resto de las clases.