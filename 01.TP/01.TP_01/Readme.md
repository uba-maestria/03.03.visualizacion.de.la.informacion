# TP 1

* Dataset: https://datos.gob.ar/dataset/ambiente-incendios-forestales/archivo/ambiente_93c2f293-9698-4457-92cf-e07856d8edb2
* Informacion de incendios, por año, por provincia
* Cantidad de incendios
	* total_incendios	
	* incendio_neglgencia	
	* incendio_intencional	
	* incendio_natural	
	* incendio_desconocida
* Conveniente recortar los años (Descartar los '90, parece que los primeros años vienen siendo medio medio)

## TODO

* Pregunta
* Unificar "Santa Fe" + "Santa Fé"
* Intentos
	* Una provincia - Evolucion de cantidad de incendios totales
	* Una provincia - Distribución de incendios por tipo (100%)
* Tipos
	* Incendios con causa desconocidas es preponderante
		* Descartarlo? Ponerlo en foco?
	* Variabilidad de incendios naturales? Distribución geográfica de los naturales?
* Incendios vs poblacion? 
* Incendios vs areas verdes
* Incendios vs temperatura 

* Intentar
	* Agrupar por provincia
		* Sumar columnas
		* Media columnas
	* Top 5 de provincias
	* 

* Datos espaciales, datos 
