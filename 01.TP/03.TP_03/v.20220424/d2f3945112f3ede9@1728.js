function _1(md,fork_button,publish_button){return(
md`# 1.5 TP-3: Introducción a Observable y D3

Por favor, para resolver los ejercicios del práctico utilizá este mismo notebook. Podés crearte una copia en tu propia cuenta de Observable haciendo click en ${fork_button()} (arriba a la derecha). Tus modificaciones a esa copia no afectarán el notebook original. Una vez terminado, no olviden apretar el botón ${publish_button()} (arriba a la derecha) para que el notebook sea público y podamos acceder para su corrección. Después de cada modificación que realicen al notebook deberán volver apretar el boton Publish si quieren que esa modificación se vuelva visible para cualquier otro usuario.

El objetivo es recrear las siguientes imágenes utilizando el canvas y las primitivas de visualización en html. Solamente el *b)* y el *d)* son obligatorios. De todas maneras, recomendamos que intenten resolverlos a todos, sobre todo si no estan muy familiarizados con la programación visual.`
)}

function _2(html){return(
html`<figure>
      <figcaption   display="block">a)</figcaption>
      <img src="https://i.imgur.com/ViSqHGt.png" width="256" height="256">
      <figcaption>b)</figcaption>
      <img src="https://i.imgur.com/GTHbT8J.png" width="256" height="256">
      <figcaption>c)</figcaption>
      <img src="https://i.imgur.com/9dHo765.png" width="256" height="256">
      <figcaption>d)</figcaption>
      <img src="https://i.imgur.com/a0qWzOt.png" width="256" height="256">
      <figcaption>e)</figcaption>
      <img src="https://i.imgur.com/LPKreJrl.png" width="256" height="256">
    </figure>`
)}

function _3(md){return(
md`Les recordamos que es posible crear círculos y polígonos de la siguiente manera:`
)}

function _4(html){return(
html`<svg width="200" height="200">
        <circle cx="100" cy="100" r="75" style="fill:green;"></circle>
        <circle cx="100" cy="100" r="50" style="fill:purple;"></circle>
        <circle cx="100" cy="100" r="25" style="fill:red;"></circle>`
)}

function _5(html){return(
html`<svg width="110" height="110">
        <polygon points="50,5 20,99 95,39 5,39 80,99" style="fill:blue;stroke:red;stroke-width:5;fill-rule:evenodd;" />
     </circle>`
)}

function _6(md){return(
md`## Ejercicio a)`
)}

function _7(md){return(
md`Creamos el array con los datos a graficar...`
)}

function _grilla_circ_conc()
{
  var coleccion = []; // Arreglo de datos a retornar
  var cantidad_circulos = 3;
  var incremento = 1;
  var rad = 100;
  var rad_dec = rad*0.25;
  var color_ = ['green', 'purple', 'red'];
  
  for(var r = 1; r < cantidad_circulos+incremento; r = r+incremento) {
      rad = rad - rad_dec
      var circle_elem = { 
                        eje_x: 100,
                        eje_y: 100, 
                        radio:  rad,
                        color: color_[r-1],
      };
    coleccion.push(circle_elem);
    }
    return coleccion;
  }


function _9(d3,grilla_circ_conc)
{
  // 1. Creación de un área de dibujo (512x90 pixeles)
  const svg = d3.create("svg")
                .attr("width",400)
                .attr("height",400);
  
    // 2. Asociamos la colección de círculos a componentes gráficos "circle"
  var circulos = svg.selectAll("circle")         // Esta selección da vacio
                    .data(grilla_circ_conc) // Estos son los datos
                    .enter()                     // Para cada entrada
                    .append("circle");           // Agregá un círculo

  // 3. Le decimos a d3 cómo utilizar la información disponible en el arreglo
  // para setear las propiedades cx, cy, r y fill del componente HTML circle
  circulos.attr("cx", function(d) {return d.eje_x;} )
          .attr("cy", function(d) {return d.eje_y;} )
          .attr("r" , function(d) {return d.radio;} )
          .style("fill", function(d) {return d.color;} )
  

  // 4. Retornamos el canvas
  return svg.node(); 
}


function _10(md){return(
md`## Ejercicio b)`
)}

function _circulo()
{
    var coleccion = []; // Arreglo de datos a retornar
    var cant = 17;
    var incremento = 1;
    var rad = 5;
    var color_ = [0, 0, 0];
    var x_pos = 10;
    var y_pos = 10;
    
    for(var i = 1; i < cant+1; i = i+1) {
      for(var j = 1; j < cant+1; j = j+1) {
          // color_[0] = color_[0] + 10;
          // color_[1] = color_[1] + 10;
          // color_[2] = color_[2] + 10;
          // x_pos = x_pos + (i*25); 
          // y_pos = y_pos + 0; 
          var circle_elem = { 
                            eje_x: x_pos + (i*15),
                            eje_y: y_pos + (j*15), 
                            radio:  rad,
                            color: "rgba("+color_[0]+(i*15)+", "+color_[1]+(j*15)+", "+color_[2]+", 1)",
          };
          coleccion.push(circle_elem);
        }
    }
    return coleccion;
  }


function _12(d3,circulo)
{
  // 1. Creación de un área de dibujo (512x90 pixeles)
  const svg = d3.create("svg")
                .attr("width",400)
                .attr("height",400);
    
  var circulos = svg.selectAll("circle")         // Esta selección da vacio
                      .data(circulo) // Estos son los datos
                      .enter()                     // Para cada entrada
                      .append("circle");           // Agregá un círculo
  
    // 3. Le decimos a d3 cómo utilizar la información disponible en el arreglo
    // para setear las propiedades cx, cy, r y fill del componente HTML circle
    circulos.attr("cx", function(d) {return d.eje_x;} )
            .attr("cy", function(d) {return d.eje_y;} )
            .attr("r" , function(d) {return d.radio;} )
            .style("fill", function(d) {return d.color;} )
    

  // 4. Retornamos el canvas
  return svg.node(); 
}


function _13(md){return(
md`Original`
)}

function _14(html){return(
html`<figure>
      <figcaption>b)</figcaption>
      <img src="https://i.imgur.com/GTHbT8J.png" width="256" height="256">
    </figure>`
)}

function _15(md){return(
md`## Ejercicio c)`
)}

function _16(d3)
{
  // 1. Creación de un área de dibujo (256x256 pixeles).
  //    Si lo consideran necesario, pueden modificar el tamaño del canvas.
  //    También es posible que necesiten más de una celda para resolverlo.
  const svg = d3.create("svg")
                .attr("width",256)
                .attr("height",256);

  
  // n. Retornamos el canvas.
  return svg.node();
}


function _17(md){return(
md`## Ejercicio d)`
)}

function _18(md){return(
md`Original`
)}

function _19(html){return(
html`<figure>
      <figcaption>d)</figcaption>
      <img src="https://i.imgur.com/a0qWzOt.png" width="256" height="256">
    </figure>`
)}

function _20(md){return(
md`Test`
)}

function _21(html){return(
html`
<svg height="400" width="1000">
  <-- Son pares x-y. Conecta cada uno. !->
  <polygon points="100 300, 300 300, 200 100, 100 300"style="fill:yellow; stroke: blue; stroke-width:2" />
  
  <polygon points="210 100, 410 100, 310 300, 210 100" style="fill:None; stroke: red; stroke-width:2" />
  <polygon points="320 300, 520 300, 420 100, 320 300"style="fill:yellow; stroke: blue; stroke-width:2" />

  <polygon points="430 100, 630 100, 530 300, 430 100" style="fill:None; stroke: red; stroke-width:2" />
  <polygon points="540 300, 740 300, 640 100, 540 300"style="fill:yellow; stroke: blue; stroke-width:2" />
</svg>
</html>`
)}

function _22(html){return(
html`
<svg height="600" width="2000">
  <-- Son pares x-y. Conecta cada uno. !->
  <polygon points="100 300, 300 300, 200 100, 100 300"style="fill:yellow; stroke: blue; stroke-width:2" />
  <polygon points="100 520, 300 520, 200 320, 100 520"style="fill:yellow; stroke: blue; stroke-width:2" />

<polygon points="210 100, 410 100, 310 300, 210 100" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="320 300, 520 300, 420 100, 320 300"style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="430 100, 630 100, 530 300, 430 100" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="540 300, 740 300, 640 100, 540 300"style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="650 100, 850 100, 750 300, 650 100" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="760 300, 960 300, 860 100, 760 300"style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="870 100, 1070 100, 970 300, 870 100" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="980 300, 1180 300, 1080 100, 980 300"style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1090 100, 1290 100, 1190 300, 1090 100" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1200 300, 1400 300, 1300 100, 1200 300"style="fill:yellow; stroke: blue; stroke-width:2" />

</svg>
</html>`
)}

function _24(html){return(
html`
<svg height="800" width="1000">
  <-- Son pares x-y. Conecta cada uno. !->
<polygon points="84 40, 164 40, 124 120, 84 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 120, 208 120, 168 40, 128 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 128, 164 128, 124 208, 84 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 208, 208 208, 168 128, 128 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 216, 164 216, 124 296, 84 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 296, 208 296, 168 216, 128 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 304, 164 304, 124 384, 84 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 384, 208 384, 168 304, 128 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 392, 164 392, 124 472, 84 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 472, 208 472, 168 392, 128 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 480, 164 480, 124 560, 84 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 560, 208 560, 168 480, 128 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 568, 164 568, 124 648, 84 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 648, 208 648, 168 568, 128 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 656, 164 656, 124 736, 84 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 736, 208 736, 168 656, 128 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 744, 164 744, 124 824, 84 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 824, 208 824, 168 744, 128 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 832, 164 832, 124 912, 84 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 912, 208 912, 168 832, 128 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 920, 164 920, 124 1000, 84 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 1000, 208 1000, 168 920, 128 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 1008, 164 1008, 124 1088, 84 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 1088, 208 1088, 168 1008, 128 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="84 1096, 164 1096, 124 1176, 84 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="128 1176, 208 1176, 168 1096, 128 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 40, 252 40, 212 120, 172 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 120, 296 120, 256 40, 216 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 128, 252 128, 212 208, 172 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 208, 296 208, 256 128, 216 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 216, 252 216, 212 296, 172 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 296, 296 296, 256 216, 216 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 304, 252 304, 212 384, 172 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 384, 296 384, 256 304, 216 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 392, 252 392, 212 472, 172 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 472, 296 472, 256 392, 216 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 480, 252 480, 212 560, 172 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 560, 296 560, 256 480, 216 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 568, 252 568, 212 648, 172 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 648, 296 648, 256 568, 216 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 656, 252 656, 212 736, 172 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 736, 296 736, 256 656, 216 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 744, 252 744, 212 824, 172 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 824, 296 824, 256 744, 216 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 832, 252 832, 212 912, 172 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 912, 296 912, 256 832, 216 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 920, 252 920, 212 1000, 172 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 1000, 296 1000, 256 920, 216 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 1008, 252 1008, 212 1088, 172 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 1088, 296 1088, 256 1008, 216 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="172 1096, 252 1096, 212 1176, 172 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="216 1176, 296 1176, 256 1096, 216 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 40, 340 40, 300 120, 260 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 120, 384 120, 344 40, 304 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 128, 340 128, 300 208, 260 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 208, 384 208, 344 128, 304 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 216, 340 216, 300 296, 260 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 296, 384 296, 344 216, 304 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 304, 340 304, 300 384, 260 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 384, 384 384, 344 304, 304 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 392, 340 392, 300 472, 260 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 472, 384 472, 344 392, 304 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 480, 340 480, 300 560, 260 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 560, 384 560, 344 480, 304 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 568, 340 568, 300 648, 260 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 648, 384 648, 344 568, 304 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 656, 340 656, 300 736, 260 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 736, 384 736, 344 656, 304 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 744, 340 744, 300 824, 260 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 824, 384 824, 344 744, 304 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 832, 340 832, 300 912, 260 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 912, 384 912, 344 832, 304 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 920, 340 920, 300 1000, 260 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 1000, 384 1000, 344 920, 304 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 1008, 340 1008, 300 1088, 260 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 1088, 384 1088, 344 1008, 304 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="260 1096, 340 1096, 300 1176, 260 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="304 1176, 384 1176, 344 1096, 304 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 40, 428 40, 388 120, 348 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 120, 472 120, 432 40, 392 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 128, 428 128, 388 208, 348 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 208, 472 208, 432 128, 392 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 216, 428 216, 388 296, 348 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 296, 472 296, 432 216, 392 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 304, 428 304, 388 384, 348 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 384, 472 384, 432 304, 392 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 392, 428 392, 388 472, 348 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 472, 472 472, 432 392, 392 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 480, 428 480, 388 560, 348 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 560, 472 560, 432 480, 392 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 568, 428 568, 388 648, 348 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 648, 472 648, 432 568, 392 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 656, 428 656, 388 736, 348 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 736, 472 736, 432 656, 392 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 744, 428 744, 388 824, 348 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 824, 472 824, 432 744, 392 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 832, 428 832, 388 912, 348 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 912, 472 912, 432 832, 392 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 920, 428 920, 388 1000, 348 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 1000, 472 1000, 432 920, 392 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 1008, 428 1008, 388 1088, 348 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 1088, 472 1088, 432 1008, 392 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="348 1096, 428 1096, 388 1176, 348 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="392 1176, 472 1176, 432 1096, 392 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 40, 516 40, 476 120, 436 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 120, 560 120, 520 40, 480 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 128, 516 128, 476 208, 436 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 208, 560 208, 520 128, 480 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 216, 516 216, 476 296, 436 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 296, 560 296, 520 216, 480 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 304, 516 304, 476 384, 436 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 384, 560 384, 520 304, 480 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 392, 516 392, 476 472, 436 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 472, 560 472, 520 392, 480 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 480, 516 480, 476 560, 436 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 560, 560 560, 520 480, 480 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 568, 516 568, 476 648, 436 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 648, 560 648, 520 568, 480 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 656, 516 656, 476 736, 436 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 736, 560 736, 520 656, 480 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 744, 516 744, 476 824, 436 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 824, 560 824, 520 744, 480 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 832, 516 832, 476 912, 436 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 912, 560 912, 520 832, 480 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 920, 516 920, 476 1000, 436 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 1000, 560 1000, 520 920, 480 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 1008, 516 1008, 476 1088, 436 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 1088, 560 1088, 520 1008, 480 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="436 1096, 516 1096, 476 1176, 436 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="480 1176, 560 1176, 520 1096, 480 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 40, 604 40, 564 120, 524 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 120, 648 120, 608 40, 568 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 128, 604 128, 564 208, 524 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 208, 648 208, 608 128, 568 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 216, 604 216, 564 296, 524 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 296, 648 296, 608 216, 568 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 304, 604 304, 564 384, 524 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 384, 648 384, 608 304, 568 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 392, 604 392, 564 472, 524 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 472, 648 472, 608 392, 568 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 480, 604 480, 564 560, 524 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 560, 648 560, 608 480, 568 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 568, 604 568, 564 648, 524 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 648, 648 648, 608 568, 568 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 656, 604 656, 564 736, 524 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 736, 648 736, 608 656, 568 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 744, 604 744, 564 824, 524 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 824, 648 824, 608 744, 568 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 832, 604 832, 564 912, 524 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 912, 648 912, 608 832, 568 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 920, 604 920, 564 1000, 524 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 1000, 648 1000, 608 920, 568 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 1008, 604 1008, 564 1088, 524 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 1088, 648 1088, 608 1008, 568 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="524 1096, 604 1096, 564 1176, 524 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="568 1176, 648 1176, 608 1096, 568 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 40, 692 40, 652 120, 612 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 120, 736 120, 696 40, 656 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 128, 692 128, 652 208, 612 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 208, 736 208, 696 128, 656 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 216, 692 216, 652 296, 612 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 296, 736 296, 696 216, 656 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 304, 692 304, 652 384, 612 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 384, 736 384, 696 304, 656 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 392, 692 392, 652 472, 612 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 472, 736 472, 696 392, 656 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 480, 692 480, 652 560, 612 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 560, 736 560, 696 480, 656 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 568, 692 568, 652 648, 612 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 648, 736 648, 696 568, 656 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 656, 692 656, 652 736, 612 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 736, 736 736, 696 656, 656 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 744, 692 744, 652 824, 612 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 824, 736 824, 696 744, 656 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 832, 692 832, 652 912, 612 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 912, 736 912, 696 832, 656 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 920, 692 920, 652 1000, 612 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 1000, 736 1000, 696 920, 656 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 1008, 692 1008, 652 1088, 612 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 1088, 736 1088, 696 1008, 656 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="612 1096, 692 1096, 652 1176, 612 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="656 1176, 736 1176, 696 1096, 656 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 40, 780 40, 740 120, 700 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 120, 824 120, 784 40, 744 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 128, 780 128, 740 208, 700 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 208, 824 208, 784 128, 744 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 216, 780 216, 740 296, 700 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 296, 824 296, 784 216, 744 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 304, 780 304, 740 384, 700 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 384, 824 384, 784 304, 744 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 392, 780 392, 740 472, 700 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 472, 824 472, 784 392, 744 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 480, 780 480, 740 560, 700 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 560, 824 560, 784 480, 744 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 568, 780 568, 740 648, 700 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 648, 824 648, 784 568, 744 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 656, 780 656, 740 736, 700 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 736, 824 736, 784 656, 744 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 744, 780 744, 740 824, 700 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 824, 824 824, 784 744, 744 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 832, 780 832, 740 912, 700 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 912, 824 912, 784 832, 744 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 920, 780 920, 740 1000, 700 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 1000, 824 1000, 784 920, 744 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 1008, 780 1008, 740 1088, 700 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 1088, 824 1088, 784 1008, 744 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="700 1096, 780 1096, 740 1176, 700 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="744 1176, 824 1176, 784 1096, 744 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 40, 868 40, 828 120, 788 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 120, 912 120, 872 40, 832 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 128, 868 128, 828 208, 788 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 208, 912 208, 872 128, 832 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 216, 868 216, 828 296, 788 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 296, 912 296, 872 216, 832 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 304, 868 304, 828 384, 788 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 384, 912 384, 872 304, 832 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 392, 868 392, 828 472, 788 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 472, 912 472, 872 392, 832 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 480, 868 480, 828 560, 788 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 560, 912 560, 872 480, 832 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 568, 868 568, 828 648, 788 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 648, 912 648, 872 568, 832 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 656, 868 656, 828 736, 788 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 736, 912 736, 872 656, 832 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 744, 868 744, 828 824, 788 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 824, 912 824, 872 744, 832 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 832, 868 832, 828 912, 788 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 912, 912 912, 872 832, 832 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 920, 868 920, 828 1000, 788 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 1000, 912 1000, 872 920, 832 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 1008, 868 1008, 828 1088, 788 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 1088, 912 1088, 872 1008, 832 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="788 1096, 868 1096, 828 1176, 788 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="832 1176, 912 1176, 872 1096, 832 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 40, 956 40, 916 120, 876 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 120, 1000 120, 960 40, 920 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 128, 956 128, 916 208, 876 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 208, 1000 208, 960 128, 920 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 216, 956 216, 916 296, 876 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 296, 1000 296, 960 216, 920 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 304, 956 304, 916 384, 876 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 384, 1000 384, 960 304, 920 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 392, 956 392, 916 472, 876 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 472, 1000 472, 960 392, 920 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 480, 956 480, 916 560, 876 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 560, 1000 560, 960 480, 920 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 568, 956 568, 916 648, 876 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 648, 1000 648, 960 568, 920 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 656, 956 656, 916 736, 876 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 736, 1000 736, 960 656, 920 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 744, 956 744, 916 824, 876 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 824, 1000 824, 960 744, 920 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 832, 956 832, 916 912, 876 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 912, 1000 912, 960 832, 920 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 920, 956 920, 916 1000, 876 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 1000, 1000 1000, 960 920, 920 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 1008, 956 1008, 916 1088, 876 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 1088, 1000 1088, 960 1008, 920 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="876 1096, 956 1096, 916 1176, 876 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="920 1176, 1000 1176, 960 1096, 920 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 40, 1044 40, 1004 120, 964 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 120, 1088 120, 1048 40, 1008 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 128, 1044 128, 1004 208, 964 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 208, 1088 208, 1048 128, 1008 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 216, 1044 216, 1004 296, 964 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 296, 1088 296, 1048 216, 1008 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 304, 1044 304, 1004 384, 964 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 384, 1088 384, 1048 304, 1008 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 392, 1044 392, 1004 472, 964 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 472, 1088 472, 1048 392, 1008 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 480, 1044 480, 1004 560, 964 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 560, 1088 560, 1048 480, 1008 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 568, 1044 568, 1004 648, 964 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 648, 1088 648, 1048 568, 1008 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 656, 1044 656, 1004 736, 964 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 736, 1088 736, 1048 656, 1008 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 744, 1044 744, 1004 824, 964 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 824, 1088 824, 1048 744, 1008 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 832, 1044 832, 1004 912, 964 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 912, 1088 912, 1048 832, 1008 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 920, 1044 920, 1004 1000, 964 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 1000, 1088 1000, 1048 920, 1008 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 1008, 1044 1008, 1004 1088, 964 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 1088, 1088 1088, 1048 1008, 1008 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="964 1096, 1044 1096, 1004 1176, 964 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1008 1176, 1088 1176, 1048 1096, 1008 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 40, 1132 40, 1092 120, 1052 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 120, 1176 120, 1136 40, 1096 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 128, 1132 128, 1092 208, 1052 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 208, 1176 208, 1136 128, 1096 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 216, 1132 216, 1092 296, 1052 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 296, 1176 296, 1136 216, 1096 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 304, 1132 304, 1092 384, 1052 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 384, 1176 384, 1136 304, 1096 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 392, 1132 392, 1092 472, 1052 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 472, 1176 472, 1136 392, 1096 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 480, 1132 480, 1092 560, 1052 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 560, 1176 560, 1136 480, 1096 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 568, 1132 568, 1092 648, 1052 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 648, 1176 648, 1136 568, 1096 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 656, 1132 656, 1092 736, 1052 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 736, 1176 736, 1136 656, 1096 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 744, 1132 744, 1092 824, 1052 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 824, 1176 824, 1136 744, 1096 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 832, 1132 832, 1092 912, 1052 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 912, 1176 912, 1136 832, 1096 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 920, 1132 920, 1092 1000, 1052 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 1000, 1176 1000, 1136 920, 1096 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 1008, 1132 1008, 1092 1088, 1052 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 1088, 1176 1088, 1136 1008, 1096 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1052 1096, 1132 1096, 1092 1176, 1052 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1096 1176, 1176 1176, 1136 1096, 1096 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 40, 1220 40, 1180 120, 1140 40" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 120, 1264 120, 1224 40, 1184 120" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 128, 1220 128, 1180 208, 1140 128" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 208, 1264 208, 1224 128, 1184 208" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 216, 1220 216, 1180 296, 1140 216" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 296, 1264 296, 1224 216, 1184 296" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 304, 1220 304, 1180 384, 1140 304" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 384, 1264 384, 1224 304, 1184 384" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 392, 1220 392, 1180 472, 1140 392" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 472, 1264 472, 1224 392, 1184 472" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 480, 1220 480, 1180 560, 1140 480" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 560, 1264 560, 1224 480, 1184 560" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 568, 1220 568, 1180 648, 1140 568" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 648, 1264 648, 1224 568, 1184 648" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 656, 1220 656, 1180 736, 1140 656" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 736, 1264 736, 1224 656, 1184 736" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 744, 1220 744, 1180 824, 1140 744" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 824, 1264 824, 1224 744, 1184 824" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 832, 1220 832, 1180 912, 1140 832" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 912, 1264 912, 1224 832, 1184 912" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 920, 1220 920, 1180 1000, 1140 920" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 1000, 1264 1000, 1224 920, 1184 1000" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 1008, 1220 1008, 1180 1088, 1140 1008" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 1088, 1264 1088, 1224 1008, 1184 1088" style="fill:yellow; stroke: blue; stroke-width:2" />
<polygon points="1140 1096, 1220 1096, 1180 1176, 1140 1096" style="fill:None; stroke: red; stroke-width:2" />
<polygon points="1184 1176, 1264 1176, 1224 1096, 1184 1176" style="fill:yellow; stroke: blue; stroke-width:2" />
</svg>

</html>`
)}

function _25()
{
    // Generación de points
    var inf_x_1 = 210;
    var inf_x_2 = 410;
    var inf_x_3 = 310;
    var inf_y = 100;
    var sup_y = 300;
    var sup_x_1 = 320;
    var sup_x_2 = 520;
    var sup_x_3 = 420;
    var factor_reducion = 0.4

    var step_x = 220;
    var step_y = 220;
    var cant = 12

    for (var i = 0; i < cant + 1; i = i + 1) {
      for (var j = 0; j < cant + 1; j = j + 1) {
        var tri_inf =
            '<polygon points="' + 
                (inf_x_1 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion +', ' + 
                (inf_x_2 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion + ', ' + 
                (inf_x_3 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + ', ' + 
                (inf_x_1 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion + '" style="fill:None; stroke: red; stroke-width:2" />'
        var tri_sup =
            '<polygon points="' + 
                (sup_x_1 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + ', ' + 
                (sup_x_2 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + ', ' + 
                (sup_x_3 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion + ', ' + 
                (sup_x_1 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + '" style="fill:yellow; stroke: blue; stroke-width:2" />'
        // console.log('Triangulo inf')
        console.log(tri_inf)
        // console.log('Triangulo sup')
        console.log(tri_sup)
        // inf_x_1 = inf_x_1 + step_x;
        // inf_x_2 = inf_x_2 + step_x;
        // inf_x_3 = inf_x_3 + step_x;
        // sup_x_1 = sup_x_1 + step_x;
        // sup_x_2 = sup_x_2 + step_x;
        // sup_x_3 = sup_x_3 + step_x;
      }
    }
}


function _triangulos()
{
    var factor_reducion = 0.4;

  // Arreglo de datos a retornar
    var coleccion = []; 
    // Generación de points
    var inf_x_1 = 210 * factor_reducion;
    var inf_x_2 = 410 * factor_reducion;
    var inf_x_3 = 310 * factor_reducion;
    var inf_y = 100 * factor_reducion;
    var sup_y = 300 * factor_reducion;
    var sup_x_1 = 320 * factor_reducion;
    var sup_x_2 = 520 * factor_reducion;
    var sup_x_3 = 420 * factor_reducion;

    var step_x = 220 * factor_reducion;
    var step_y = 220 * factor_reducion;
    var cant = 15

    for (var i = 0; i < cant + 1; i = i + 1) {
      for (var j = 0; j < cant + 1; j = j + 1) {
        var tri_inf =
            '<polygon points="' + 
                (inf_x_1 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion +', ' + 
                (inf_x_2 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion + ', ' + 
                (inf_x_3 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + ', ' + 
                (inf_x_1 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion + '" style="fill:None; stroke: red; stroke-width:2" />'
        var tri_sup =
            '<polygon points="' + 
                (sup_x_1 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + ', ' + 
                (sup_x_2 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + ', ' + 
                (sup_x_3 + step_x * i) * factor_reducion + ' ' +
                (inf_y + step_x * j) * factor_reducion + ', ' + 
                (sup_x_1 + step_x * i) * factor_reducion + ' ' +
                (sup_y + step_x * j) * factor_reducion + '" style="fill:yellow; stroke: blue; stroke-width:2" />'
        // console.log('Triangulo inf')
        // console.log(tri_inf)
        // console.log('Triangulo sup')
        // console.log(tri_sup)
        var polygon_elem_inf = { puntos: (inf_x_1 + step_x * i) + ' ' +
                                         (inf_y + step_x * j) +', ' + 
                                         (inf_x_2 + step_x * i) + ' ' +
                                         (inf_y + step_x * j) + ', ' + 
                                         (inf_x_3 + step_x * i) + ' ' +
                                         (sup_y + step_x * j) + ', ' + 
                                         (inf_x_1 + step_x * i) + ' ' +
                                         (inf_y + step_x * j),
                                 fill: 'None', 
                                 stroke:  'red',
                                 strokeWidth: 2,
                               };
        coleccion.push(polygon_elem_inf);
        var polygon_elem_sup = { 
                            puntos: (sup_x_1 + step_x * i) + ' ' +
                                    (sup_y + step_x * j) + ', ' + 
                                    (sup_x_2 + step_x * i) + ' ' +
                                    (sup_y + step_x * j) + ', ' + 
                                    (sup_x_3 + step_x * i) + ' ' +
                                    (inf_y + step_x * j) + ', ' + 
                                    (sup_x_1 + step_x * i) + ' ' +
                                    (sup_y + step_x * j),
                            fill: 'yellow', 
                            stroke:  'blue',
                            strokeWidth: 2,
        };
        coleccion.push(polygon_elem_sup);

        // inf_x_1 = inf_x_1 + step_x;
        // inf_x_2 = inf_x_2 + step_x;
        // inf_x_3 = inf_x_3 + step_x;
        // sup_x_1 = sup_x_1 + step_x;
        // sup_x_2 = sup_x_2 + step_x;
        // sup_x_3 = sup_x_3 + step_x;
      }
    }
    return coleccion;
}


function _dibujarTriangulos(){return(
function(svg, arrayTriangulos) {
  // Asociar colección de triángulos a componentes gráficos "poligon"
  var triangulos = svg.selectAll("polygon")
                      .data(arrayTriangulos)
                      .enter()
                      .append("polygon");
  
  // Para cada elemento del array, crear un nuevo triángulos con los atributos del elemento.
  triangulos.attr("points", function(d) { return d.puntos;})
        .attr("stroke", function(d) { return d.stroke;})
        .attr("stroke-width", function(d) { return d.strokeWidth;})
        .attr("fill", function(d) { return d.fill;});
  
  // Devolver nodo SVG
  return svg.node();
}
)}

function _28(d3,dibujarTriangulos,triangulos)
{
  const svg = d3.create("svg")
                .attr("width", 500)
                .attr("height", 500);
  return dibujarTriangulos(svg, triangulos);
}


function _29(md){return(
md`## Ejercicio e)`
)}

function _30(d3)
{
  // 1. Creación de un área de dibujo (256x256 pixeles).
  //    Si lo consideran necesario, pueden modificar el tamaño del canvas.
  //    También es posible que necesiten más de una celda para resolverlo.
  const svg = d3.create("svg")
                .attr("width",256)
                .attr("height",256);
  
     
  
  // n. Retornamos el canvas.
  return svg.node();
}


function _hexagon(){return(
function hexagon(x, y, r) {
  var x1 = x;
  var y1 = y - r;
  var x2 = x + Math.cos(Math.PI / 6) * r;
  var y2 = y - Math.sin(Math.PI / 6) * r;
  var x3 = x + Math.cos(Math.PI / 6) * r;
  var y3 = y + Math.sin(Math.PI / 6) * r;
  var x4 = x;
  var y4 = y + r;
  var x5 = x - Math.cos(Math.PI / 6) * r;
  var y5 = y + Math.sin(Math.PI / 6) * r;
  var x6 = x - Math.cos(Math.PI / 6) * r;
  var y6 = y - Math.sin(Math.PI / 6) * r;

  var path = x1 + ',' + y1 + " " + x2 + ',' + y2 + " " + x3 + ',' + y3 + " " + x4 + ',' + y4 + " " + x5 + ',' + y5 + " " + x6 + ',' + y6;
  return path;
}
)}

function _32(md){return(
md`### Otras funciones auxiliares `
)}

function _d3(require){return(
require("d3@5")
)}

function _fork_button(html){return(
function fork_button() { return html`<button type="button" class="no-underline pointer inline-flex fw6 dark-gray hover-near-black bg-silver hover-bg-light-silver br2 pa2 bn f6 bg-animate  mr2 "><svg width="16" height="16" viewBox="0 0 16 16" fill="none" class="w1 h1 mr1-ns"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.5 1.75C3.80964 1.75 3.25 2.30964 3.25 3C3.25 3.69036 3.80964 4.25 4.5 4.25C5.19036 4.25 5.75 3.69036 5.75 3C5.75 2.30964 5.19036 1.75 4.5 1.75ZM1.75 3C1.75 1.48122 2.98122 0.25 4.5 0.25C6.01878 0.25 7.25 1.48122 7.25 3C7.25 4.16599 6.52434 5.1625 5.5 5.56253V7H8.5C9.4199 7 10.1947 6.37895 10.4281 5.53327C9.44188 5.11546 8.75 4.13853 8.75 3C8.75 1.48122 9.98122 0.25 11.5 0.25C13.0188 0.25 14.25 1.48122 14.25 3C14.25 4.18168 13.5047 5.18928 12.4585 5.57835C12.1782 7.51343 10.5127 9 8.5 9H5.5V10.4375C6.52434 10.8375 7.25 11.834 7.25 13C7.25 14.5188 6.01878 15.75 4.5 15.75C2.98122 15.75 1.75 14.5188 1.75 13C1.75 11.834 2.47566 10.8375 3.5 10.4375L3.5 9V7V5.56253C2.47566 5.1625 1.75 4.16599 1.75 3ZM4.5 11.75C3.80964 11.75 3.25 12.3096 3.25 13C3.25 13.6904 3.80964 14.25 4.5 14.25C5.19036 14.25 5.75 13.6904 5.75 13C5.75 12.3096 5.19036 11.75 4.5 11.75ZM10.25 3C10.25 2.30964 10.8096 1.75 11.5 1.75C12.1904 1.75 12.75 2.30964 12.75 3C12.75 3.69036 12.1904 4.25 11.5 4.25C10.8096 4.25 10.25 3.69036 10.25 3Z" fill="currentColor"></path></svg><span class="db">Fork</span></button>`}
)}

function _publish_button(html){return(
function publish_button() { return html`<button title="This notebook has been edited since publishing." class="bn bg-animate br2 pv2 ph2 f6 inline-flex fw6 mr2 white bg-blue hover-bg-light-blue pointer"><svg width="16" height="16" viewBox="0 0 16 16" fill="none" class="w1 h1 mr1"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.29289 4.70711L7.29289 1.70711L8 1L8.70711 1.70711L11.7071 4.70711L10.2929 6.12132L9 4.82843V10H7V4.82843L5.70711 6.12132L4.29289 4.70711ZM3 15C1.89543 15 1 14.1046 1 13V8C1 6.89543 1.89543 6 3 6V8V13H13V8V6C14.1046 6 15 6.89543 15 8V13C15 14.1046 14.1046 15 13 15H3Z" fill="currentColor"></path></svg><span class="db">Publish</span></button>`}
)}

export default function define(runtime, observer) {
  const main = runtime.module();
  main.variable(observer()).define(["md","fork_button","publish_button"], _1);
  main.variable(observer()).define(["html"], _2);
  main.variable(observer()).define(["md"], _3);
  main.variable(observer()).define(["html"], _4);
  main.variable(observer()).define(["html"], _5);
  main.variable(observer()).define(["md"], _6);
  main.variable(observer()).define(["md"], _7);
  main.variable(observer("grilla_circ_conc")).define("grilla_circ_conc", _grilla_circ_conc);
  main.variable(observer()).define(["d3","grilla_circ_conc"], _9);
  main.variable(observer()).define(["md"], _10);
  main.variable(observer("circulo")).define("circulo", _circulo);
  main.variable(observer()).define(["d3","circulo"], _12);
  main.variable(observer()).define(["md"], _13);
  main.variable(observer()).define(["html"], _14);
  main.variable(observer()).define(["md"], _15);
  main.variable(observer()).define(["d3"], _16);
  main.variable(observer()).define(["md"], _17);
  main.variable(observer()).define(["md"], _18);
  main.variable(observer()).define(["html"], _19);
  main.variable(observer()).define(["md"], _20);
  main.variable(observer()).define(["html"], _21);
  main.variable(observer()).define(["html"], _22);
  main.variable(observer()).define(["html"], _24);
  main.variable(observer()).define(_25);
  main.variable(observer("triangulos")).define("triangulos", _triangulos);
  main.variable(observer("dibujarTriangulos")).define("dibujarTriangulos", _dibujarTriangulos);
  main.variable(observer()).define(["d3","dibujarTriangulos","triangulos"], _28);
  main.variable(observer()).define(["md"], _29);
  main.variable(observer()).define(["d3"], _30);
  main.variable(observer("hexagon")).define("hexagon", _hexagon);
  main.variable(observer()).define(["md"], _32);
  main.variable(observer("d3")).define("d3", ["require"], _d3);
  main.variable(observer("fork_button")).define("fork_button", ["html"], _fork_button);
  main.variable(observer("publish_button")).define("publish_button", ["html"], _publish_button);
  return main;
}
