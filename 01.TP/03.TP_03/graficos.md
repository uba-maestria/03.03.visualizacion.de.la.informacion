# Tp 3

Enunciado: 

  Por favor, para resolver los ejercicios del práctico utilizá este mismo notebook. 
  Podés crearte una copia en tu propia cuenta de Observable haciendo click en Fork (arriba a la derecha). Tus modificaciones a esa copia no afectarán el notebook original. Una vez terminado, no olviden apretar el botón Publish (arriba a la derecha) para que el notebook sea público y podamos acceder para su corrección. 
  Después de cada modificación que realicen al notebook deberán volver apretar el boton Publish si quieren que esa modificación se vuelva visible para cualquier otro usuario.


Link: https://observablehq.com/@blukitas/1-5-tp-3-introduccion-a-observable-y-d3

