# Clase 2

Primera clase teórica

## Intro y objetivos

* Circulo heumeneutico - Saassure
* Signos visuales - Dan pautas
* Edward Tufte 
* Kean - 

* La mayoría de las tareas cognitivas humanas se apoya en el sentido de la vista y de su capacidad para generar abstracciones, conceptualizaciones, relaciones
	* Alrededor del 70% de la información que recibimos es visual (El 30% es el resto)
	* 10K más, de lo auditivo
	* Hay un correlato biológico. Hay neuronas que se disparan frente a eventos y formas simples (Rectas, cosas que se mueven)

	Data -> Information -> Knowledge -> Insight -> Wisdom

* El objetivo de la visualizacion es el insight (El soporte a la toma de decisiones)	
	* TODO: Check
	* La visualizacion tiene que ser el soporte para contar una historia. Puede ser breve. Pero es importante, puede ser importantisima.
* Otra vertiente dice que la visualizacion tiene que generarte un modelo en la cabeza, que de otra forma sería muy diferente (O imposible).

* Affordance 
	* Que tan facil de aprehender es algo
	* Teoria que anda dando vueltas en UX (Entre otros). Parece interesante  

* Ken robbinson -> El sistema educativo viene del modelo industrial
	* Pensar que hay un centro semántico es llevar está idea muy lejos -> Pero no es así. 

* Tipos de personas
	* Algebraicas
	* 

* Gestalt
	* Cuando algo salta a la vista, se vuelve semánticamente hegemonico. 
	* Cuando vez algo se vuelve predominante, y no se puede ver otra cosa
* Modelo SPECT (?)

## Historia de las Viz

* El primer registro de visualización son los mapas en egipto (Circa 1150 a.c.)
	* El mapa no es un territorio, es la metáfora del territorio (Sassure)
	* Metáfora visual. Donde cada punto del mapa representa algun punto/región del territorio
* El espacio es la referencia favorita
	* graficar la relacion x-y [ y = f(x) ] -> Es una forma de representar en el espacio la relación
* Representacion de posiciones planetarias (Aprox 900 d.c.)
	* Graficar los movimientos planetarios (Planeta - tiempo - traslación)
	* Salto en el pensamiento. Agregar el tiempo como una variable.
* Redes semánticas - Raimundo Lull (1300 d.c.)
	* Representar la semántica -> Establecer un sistema de pensamiento (Religioso, filosofico)
	* (Él) Agregaría la "caballa"
* Charles de Fourcroy (1800 d.c.)
	* Grafico para mostrar la tabla demográfica de las principales ciudades europeas
	* Estadística viene de estado -> Datos para el estado, usar los numeros para tomar decisiones de políticas de estado
	* Nace en europa, en inglaterra (En medio de la ilustración)
		* Muestreo los campos de trigo y con eso decido si tengo que importar trigo (O si puedo exportar)
* Charles Minard /1820 d.c) -> Campaña de Napoleón
	* Gráfico ostensible
* Grafico Jhon Snow - Identificar las causas del cólera en inglaterra
* Florence nightingale - Causas de la mortalidad en la campaña del ejército en 1856

## Modelos conceptuales

1) Modelos conceptuales de la InfoViz: Colin Ware
2) Modelos conceptuales de la InfoViz: Ben Fry
3) Modelos conceptuales de la InfoViz: Ben Schneiderman
4) Modelos conceptuales de la InfoViz: Haber y McNabb
5) Modelos conceptuales de la InfoViz: Brodlie
6) Modelos conceptuales de la InfoViz: Buja
	* Gestalt
		* Proximidad
		* Similitud
		* Clausura
		* Continuación
		* Organización figura-fondo
	* Trabajo con la bauhaus también
	* Punto y linea sobre el plano de kandinsky -> elementos visuales y, lo que el llama, énfasis


### Modelos conceptuales de la InfoViz: Una categorización de los mapeos

* Variado
* Valuado

## Otros?

- Vivir en un mundo de signos y no de significados.

- ¿Es el scroll del pdf una visualizacion?
- Si, es una visualizacion interactiva
- ¿Cual es la variable independiente, dependiente y ...?
- Donde empieza y termina el scroll (La cajita del scroll). La cajita representa la parte del mapa que está visible (Parte del documento en la pantalla).
